package dao;

import controller.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import util.HibernateUtil;

public class BookDao {

    public void createBook(Book book) {
        Transaction transaction = null;

        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();

        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public Book getBook(int bookId) {
        try {
            Book book;
            SessionFactory sf = HibernateUtil.getSessionFactory();
            Session session = sf.openSession();
            book = session.get(Book.class, bookId);
            return book;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public void updateBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
// start a transaction
            transaction = session.beginTransaction();
// save the person object
            session.update(book);
// commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }

    public void deleteBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtil.getSessionFactory().openSession();
// start a transaction
            transaction = session.beginTransaction();
// save the person object
            session.delete(book);
// commit transaction
            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
        }
    }
}
