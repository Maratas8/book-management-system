import controller.Book;
import dao.BookDao;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import util.HibernateUtil;

public class Main extends Application {

    private Scene scene;

    public static void main(String[] args) {
        HibernateUtil hibernateUtil = new HibernateUtil();
        BookDao bookDAO = new BookDao();
        Book book1 = new Book("name1", "author1", "2004", 434);
        bookDAO.createBook(book1);
        Book book2 = new Book("name2", "author2", "2006", 332);
        bookDAO.createBook(book2);
        Main.launch();

        hibernateUtil.shutdown();

    }

    @Override
    public void init() throws Exception {
        System.out.println("Initializing app");
    }

    @Override
    public void start(Stage primaryStage) {
        System.out.println("Starting app");

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/view.fxml"));
            Scene scene = new Scene(root, 600, 400);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    @Override
    public void stop() throws Exception {
        // this will be executed even if Exception is thrown at runtime
        System.out.println("Stopping app");
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Scene getScene() {
        return this.scene;
    }

}
