package controller;

import dao.BookDao;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.stage.Stage;

public class ViewController {

    @FXML
    private void onShowLibraryClick() {
        System.out.println("Show library");
    }

    @FXML
    private void onAddBookClick() {

        try {
            Parent root = FXMLLoader.load(getClass().getResource("/addBook.fxml"));
            Scene scene = new Scene(root, 600, 400);
            Stage secondStage = new Stage();
            secondStage.setScene(scene);
            secondStage.show();

        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println("Add book!");
    }

    @FXML
    private void onUpdateBookClick() {
        System.out.println("Update book");
    }

    @FXML
    private void onDeleteBookClick() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/deleteBook.fxml"));
            Scene scene = new Scene(root);
            Stage deleteStage = new Stage();
            deleteStage.setScene(scene);
            deleteStage.show();

        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("Delete book");
    }

    @FXML
    private void onExitClick() {
        Platform.exit();
        System.out.println("Exit");

    }

    @FXML
    private void initialize() {
        System.out.println("Controller is initialized!");
    }
}
